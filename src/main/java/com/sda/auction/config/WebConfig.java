package com.sda.auction.config;

import com.sda.auction.service.ItemService;
import com.sda.auction.service.impl.ItemServiceImpl;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;

@Configuration
public class WebConfig {
    //folosim @Bean pentru ca folosim o functionalitate externa, nu avem noi clasa BCryptPasswordEncoder creata in proiectul nostru
    //cand vom pune @Autowired pe obiectul bCryptPasswordEncoder el va veni si il va cauta aici, pt ca are @Bean
    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
        return bCryptPasswordEncoder;
    }

//incarca mesajele de eroare din fisierul de text
    @Bean(name = "messageSource")
    public MessageSource messageSource() {
        ReloadableResourceBundleMessageSource messageSource =
                new ReloadableResourceBundleMessageSource();
        messageSource.setBasename("classpath:/validation/messages");
        return messageSource;
    }
    //bean ia informatiile de la messageSource. messageSource raspunde cu ce gaseste in fisierul de messages de mai sus
    @Bean
    public LocalValidatorFactoryBean validator(MessageSource messageSource) {
        LocalValidatorFactoryBean bean = new LocalValidatorFactoryBean();
        bean.setValidationMessageSource(messageSource);
        return bean;
    }
}
