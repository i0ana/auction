package com.sda.auction.controller;

import com.sda.auction.controller.dto.BidForm;
import com.sda.auction.controller.dto.ItemForm;
import com.sda.auction.service.BidService;
import com.sda.auction.service.ItemService;
import com.sda.auction.validator.BidFormValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("account")
//adnotare pentru a prelua tot ce vine din request cu /account.
// in loc de //account/home, scirem doar /home si va sti sa caute in controllerul de Account
public class AccountController {
    @Autowired
    private ItemService itemService;

    //nu e suficient sa ii punem doar @Autowired; trebuie sa mergem si in BidServiceImpl @Service pt a sti de unde sa instatieze
    @Autowired
    private BidService bidService;

    @Autowired
    private BidFormValidator bidFormValidator;

    @RequestMapping(value = {"/", ""}, method = RequestMethod.GET)
    public ModelAndView home() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("redirect:/account/home");
        return modelAndView;
    }


    @RequestMapping(value = {"/home",}, method = RequestMethod.GET)
    public ModelAndView accountHome() {
        ModelAndView modelAndView = new ModelAndView();
        List<ItemForm> itemList = itemService.findAvailableItem();
        modelAndView.addObject("itemList", itemList);
        modelAndView.setViewName("account/home");
        return modelAndView;
    }

    @RequestMapping(value = {"/item/{itemId}",}, method = RequestMethod.GET)
    public ModelAndView viewItemPage(@PathVariable(value = "itemId") String itemId) {
        ModelAndView modelAndView = new ModelAndView();

        ItemForm itemForm = itemService.findItemFormById(itemId);
        modelAndView.addObject(itemForm);
        //trimitem un bidform gol pt a fi pregatit sa ia informtia cand vom da Bid si va face POST
        modelAndView.addObject(new BidForm());
        modelAndView.setViewName("account/viewItem");
        return modelAndView;
    }

    @RequestMapping(value = {"/item/{itemId}"}, method = RequestMethod.POST)
    public ModelAndView viewItemPagePost(@PathVariable(value = "itemId") String itemId, @Valid BidForm bidForm) {
        ModelAndView modelAndView = new ModelAndView();
        System.out.println("!!!!!!" + bidForm);

        //redirect: Spring, nu mai cauta JSP, ci imi face redirect catre avvount/viewItem
        if (bidFormValidator.isValid(bidForm, itemId)) {
            bidService.save(bidForm, itemId);
            modelAndView.setViewName("redirect:/account/item/" + itemId);
        } else {
            modelAndView.addObject("errorMessage", "Bid not valid!");
            ItemForm itemForm = itemService.findItemFormById(itemId);
            modelAndView.addObject(itemForm);
            modelAndView.setViewName("account/viewItem");
        }
        return modelAndView;
    }
}

