package com.sda.auction.controller;

        import com.sda.auction.controller.dto.BidForm;
        import com.sda.auction.controller.dto.ItemForm;
        import com.sda.auction.service.ItemService;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.stereotype.Controller;
        import org.springframework.validation.BindingResult;
        import org.springframework.web.bind.annotation.PathVariable;
        import org.springframework.web.bind.annotation.RequestMapping;
        import org.springframework.web.bind.annotation.RequestMethod;
        import org.springframework.web.servlet.ModelAndView;

        import javax.validation.Valid;
        import java.util.List;

@Controller
@RequestMapping("admin")
public class AdminController {

    @Autowired
    private ItemService itemService;

    @RequestMapping(value = {"/", ""}, method = RequestMethod.GET)
    public ModelAndView home() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("redirect:/admin/home");
        return modelAndView;
    }

    @RequestMapping(value = {"/home",}, method = RequestMethod.GET)
    public ModelAndView adminHome() {
        ModelAndView modelAndView = new ModelAndView();
        //ca sa ne afiseze toata lista de itemi, intoarcem aceasta lista
        List<ItemForm> itemList = itemService.findAll();
        //daca intoarcem o list in addObject, trebuie sa dam si numele listei
        modelAndView.addObject("itemList", itemList);
        modelAndView.setViewName("admin/home");
        return modelAndView;
    }

    @RequestMapping(value = {"/newItem",}, method = RequestMethod.GET)
    public ModelAndView newItem() {
        ModelAndView modelAndView = new ModelAndView();
        ItemForm itemForm = new ItemForm();
        modelAndView.addObject(itemForm);
        modelAndView.setViewName("admin/newItem");
        return modelAndView;
    }


    @RequestMapping(value = {"/newItem",}, method = RequestMethod.POST)
    public ModelAndView newItem(@Valid ItemForm itemForm, BindingResult bindingResult) {
        ModelAndView modelAndView = new ModelAndView();
        if (!bindingResult.hasErrors()) {
            itemService.saveItem(itemForm);
            modelAndView.addObject("successMessage",
                    "Good job! Item saved!");
            modelAndView.addObject(new ItemForm());
        }
        modelAndView.setViewName("admin/newItem");
        return modelAndView;
    }

    @RequestMapping(value = {"/item/{itemId}",}, method = RequestMethod.GET)
    public ModelAndView viewItemPage(@PathVariable(value = "itemId") String itemId) {
        ModelAndView modelAndView = new ModelAndView();

        ItemForm itemForm = itemService.findItemFormById(itemId);
        modelAndView.addObject(itemForm);
        //trimitem un bidform gol pt a fi pregatit sa ia informtia cand vom da Bid si va face POST
        modelAndView.addObject(new BidForm());
        modelAndView.setViewName("admin/viewItem");
        return modelAndView;
    }
}
