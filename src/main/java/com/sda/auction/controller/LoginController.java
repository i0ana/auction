package com.sda.auction.controller;


import com.sda.auction.controller.dto.UserForm;
import com.sda.auction.model.User;
import com.sda.auction.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;


import javax.validation.Valid;

@Controller
public class LoginController {

    //autoWired managed by Spring
    @Autowired
    private UserService userService;


    @RequestMapping(value = {"/", "/login"}, method = RequestMethod.GET)
    public ModelAndView login() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("login");
        return modelAndView;
    }

    @RequestMapping(value = {"/registration"}, method = RequestMethod.GET)
    public ModelAndView register() {
        ModelAndView modelAndView = new ModelAndView();
        UserForm userForm = new UserForm();
        modelAndView.addObject(userForm);
        modelAndView.setViewName("registration");
        return modelAndView;
    }

    @RequestMapping(value = {"/registration"}, method = RequestMethod.POST)
    public ModelAndView registerPost(@Valid UserForm userForm, BindingResult bindingResult) {
        ModelAndView modelAndView = new ModelAndView();
        if (bindingResult.hasErrors()) {
            System.out.println("Error!");
        } else {
            User existingUser = userService.findByEmail(userForm.getEmail());
            if (existingUser != null) {
                //eroare
                bindingResult.rejectValue("email", "error.user", "There is already a user registered with this email");
            } else {
                userService.saveUser(userForm);
                modelAndView.addObject(new UserForm());
                modelAndView.addObject("successMessage", "Welcome, " + userForm.getFirstName());
            }
        }

        modelAndView.setViewName("registration"); //"registration" ma trimite pe pagina de registration inapoi, si foloseste jsp-ul nostru din WEB_INF
        return modelAndView;
    }


    @RequestMapping(value = {"/successfulLogin",}, method = RequestMethod.GET)
    public ModelAndView loginSuccessful() {
        ModelAndView modelAndView = new ModelAndView();
        if (userService.isLoggedUserAdmin()) {
            modelAndView.setViewName("redirect:/admin/home");
        } else {
            modelAndView.setViewName("redirect:/account/home");
        }
        return modelAndView;
    }




}
