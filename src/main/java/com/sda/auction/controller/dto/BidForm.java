package com.sda.auction.controller.dto;

import lombok.Data;

@Data
public class BidForm {

    private int itemId;
    private int value;



}
