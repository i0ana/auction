package com.sda.auction.controller.dto;

import lombok.Data;
import lombok.ToString;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.PositiveOrZero;

@ToString
@Data
public class ItemForm {

    private int id;

    @NotEmpty(message = "{error.item.name.empty}")
    private String itemName;
    @NotEmpty(message = "{error.item.description.empty}")
    private String description;
    @PositiveOrZero (message = "{error.item.startPrice.positive}")
    private int startingPrice;
    @NotEmpty(message = "{error.item.startDate.empty}")
    private String startDateForBidding;
    @NotEmpty (message = "{error.item.endDate.empty}")
    private String endDateForBidding;

    private String category;

    private int loggedUserBidValue;

    private int currentPrice;

    private boolean auctioned;

    private String ownerName;

}
