package com.sda.auction.controller.dto;

import com.sda.auction.validator.ConfirmPasswordConstraint;
import lombok.Data;
import lombok.ToString;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

@ToString
@Data
@ConfirmPasswordConstraint(message = "Confirm password doesn't match!")
//ne construim un validator pentru a valida daca parola = confirm password.
// punem validator la nivel de clasa pt c nu putem sa facem asta individual, pe variabila in sine, assa ca trebuie sa analizam de la un nivel mai sus
public class UserForm {
    @NotEmpty(message = "{error.user.first.name.empty}")
    private String firstName;
    @NotEmpty(message = "{error.user.last.name}")
    private String lastName;
    @Email(message = "{error.user.email.validation}")
    @NotEmpty(message = "{error.user.email.empty}")
    private String email;
    @Length(min = 4, message = "{error.user.password.length}")
    private String password;
    private String confirmPassword;


    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getEmail() {
        return email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }
}
