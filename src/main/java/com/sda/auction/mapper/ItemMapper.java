package com.sda.auction.mapper;

import com.sda.auction.controller.dto.ItemForm;
import com.sda.auction.model.Item;
import com.sda.auction.util.DateConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component
public class ItemMapper {


    private final DateConverter dateConverter;

    @Autowired
    public ItemMapper(DateConverter dateConverter) {
        this.dateConverter = dateConverter;
    }

    public Item map(ItemForm itemForm) {
        Item item = new Item();
        item.setItemName(itemForm.getItemName());
        item.setDescription(itemForm.getDescription());
        item.setCategory(itemForm.getCategory());
        item.setStartingPrice(itemForm.getStartingPrice());

        Date startDate = dateConverter.parse(itemForm.getStartDateForBidding());
        item.setStartDateForBidding(startDate);

        Date endDate = dateConverter.parse(itemForm.getEndDateForBidding());
        item.setEndDateForBidding(endDate);

        return item;
    }

    public ItemForm map(Item item) {
        ItemForm itemForm = new ItemForm();
        itemForm.setItemName(item.getItemName());
        itemForm.setDescription(item.getDescription());
        itemForm.setStartingPrice(item.getStartingPrice());
        itemForm.setCategory(item.getCategory());
        itemForm.setId(item.getId());
        itemForm.setCurrentPrice(item.currentPrice());
        itemForm.setAuctioned(!item.getBids().isEmpty());
        itemForm.setOwnerName(item.getUserName());

        String startDate = dateConverter.format(item.getStartDateForBidding());
        itemForm.setStartDateForBidding(startDate);

        String endDate = dateConverter.format(item.getEndDateForBidding());
        itemForm.setEndDateForBidding(endDate);


        return itemForm;
    }

    public List<ItemForm> map(List<Item> itemList) {
        List<ItemForm> result = new ArrayList<>();
        for (Item item : itemList) {
            ItemForm itemForm = map(item);
            result.add(itemForm);
        }

        return result;
    }
}