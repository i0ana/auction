package com.sda.auction.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.*;

@Entity
@Table(name = "bid")
@Data
@EqualsAndHashCode (exclude = {"user", "item"})
public class Bid {
    @Id
    @GeneratedValue (strategy = GenerationType.AUTO)
    @Column (name = "bid_id")
    private int id;

    @Column
    private int value;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.MERGE)
    @JoinColumn(name = "user_id")
    @ToString.Exclude
    private User user;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.MERGE)
    @JoinColumn(name = "item_id")
    @ToString.Exclude
    private Item item;

}
