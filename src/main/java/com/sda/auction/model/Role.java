package com.sda.auction.model;

import lombok.Data;

import javax.persistence.*;

@Data //ne face automat getter si setter
@Entity
@Table (name="role")
public class Role {
    @Id
    @GeneratedValue (strategy = GenerationType.AUTO)
    @Column(name = "role_id")
    private int id;

    @Column (name = "role")
    private String role;

}
