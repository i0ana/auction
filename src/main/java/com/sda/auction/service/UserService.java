package com.sda.auction.service;

import com.sda.auction.controller.dto.UserForm;
import com.sda.auction.model.User;

//contine toate serviciile care sunt legate de user. Expune ce anume putem face cu un user
//avem interfata pentru a fi totul mai clean. e mult mai usor sa te uiti la un service cu 20 de metode clean, decat 20 de metode implementate
//detaliile de implementare sunt incapsulate in clasa UserServiceImpl
//controlerul va fi legat de interfata
public interface UserService {

    void saveUser (UserForm userForm);

    User findByEmail(String email);

    String getAuthenticatedEmail();

    User getLoggedInUser();

    boolean isLoggedUserAdmin();
}
