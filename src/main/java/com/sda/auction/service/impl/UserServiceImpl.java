package com.sda.auction.service.impl;

import com.sda.auction.controller.dto.UserForm;
import com.sda.auction.mapper.UserMapper;
import com.sda.auction.model.Role;
import com.sda.auction.model.User;
import com.sda.auction.repository.RoleRepository;
import com.sda.auction.repository.UserRepository;
import com.sda.auction.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Collection;

//contne detalii de implementare. Controllerul vede doar UserService ca interfata, stie ca poate salva, edita un user,
// dar nu stie exact care sunt metodele.aceastea sunt pastrate in clasa de Impl
//clasa ServiceImpl are legatura cu Repository, niciodata Controllerul direct
//ca sa obtinem un user ca entitate, trebuie sa folosim un Model (modelMapper care transforma din DTO in Entity si invers)
@Service
public class UserServiceImpl implements UserService {
    private UserRepository userRepository;
    private UserMapper userMapper;
    private RoleRepository roleRepository;
    private BCryptPasswordEncoder passwordEncoder;

    @Autowired
    public UserServiceImpl (UserRepository userRepository, UserMapper userMapper, BCryptPasswordEncoder passwordEncoder, RoleRepository roleRepository) {
        this.userRepository = userRepository;
        this.userMapper = userMapper;
        this.passwordEncoder = passwordEncoder;
        this.roleRepository = roleRepository;
    }
    @Override
    public void saveUser(UserForm userForm) {
        User user = userMapper.map(userForm);
        encodePassword(userForm, user);
        assignUserRole(user);
        makeUserActive(user);
        userRepository.save(user);
    }

    @Override
    public User findByEmail(String email) {
        return userRepository.findByEmail(email);
    }


    private void makeUserActive(User user) {
        user.setActive(1);
    }

    private void assignUserRole(User user) {
        Role adminRole = roleRepository.findByRole("ADMIN");
        Role userRole = roleRepository.findByRole("USER");
        user.addRole(adminRole);
        user.addRole(userRole);
    }

    //primim parola in plain text si o codam, apoi o asignam intr-o variabila.
    // Dupa, variabila respectiva va inlocui parola data de la tastatura de user si se va salva in baza de data deja criptata
    private void encodePassword(UserForm userForm, User user) {
        String encodedPassword = passwordEncoder.encode(userForm.getPassword());
        user.setPassword(encodedPassword);
    }
//ajuta la identificarea username-ului care a facut bid
    public String getAuthenticatedEmail() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return authentication.getName();
    }

    @Override
    public User getLoggedInUser() {
        String userEmail = getAuthenticatedEmail();
        return findByEmail(userEmail);
    }

    @Override
    public boolean isLoggedUserAdmin() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        Collection<? extends GrantedAuthority> authorities = authentication.getAuthorities();
        return authorities.contains(new SimpleGrantedAuthority("ADMIN"));

    }
}
