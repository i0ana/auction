$(document).ready(function () {
    // $("p.error > span").parent().prev().css({ "border": "1px solid red" });

    $("p.error > span").parent().prev().addClass("error");

    $("#password").focusout(validateConfirmPassword);
    $("#confirmPassword").focusout(validateConfirmPassword);
    $("#firstName").focusout(hideErrorFirstName);
    $("#lastName").focusout(hideErrorLastName);
    $("#email").focusout(hideErrorEmail);
});

function validateConfirmPassword() {
    var passwordText = $("#password").val();
    var confirmPasswordText = $("#confirmPassword").val();
    if (confirmPasswordText.length == 0) {
        return false;
    }

    if (passwordText !== confirmPasswordText) {
        $("#confirmPassword").addClass("error").next("p.error")
            .html("<span>Passwords do not match</span>");
    } else {
        $("#confirmPassword").removeClass("error").next("p.error")
            .html("");
    }
}

function hideErrorFirstName() {
    var firstName = $("#firstName").val();
    if (firstName !== 0) {
        $("#firstName").removeClass("error").next("p.error").html("");
    }
}

function hideErrorLastName() {
    if ($("#lastName").val() !== 0) {
        $("#lastName").removeClass("error").next("p.error").html("");
    }
}

function hideErrorEmail() {
    if ($("#email").val() !== 0) {
        $("#email").removeClass("error").next("p.error").html("");
    }
}

