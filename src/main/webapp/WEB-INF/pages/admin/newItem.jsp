<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
    <html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title></title>
    <link rel="stylesheet" type="text/css" href="/css/newItem.css">
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
            integrity="sha256-pasqAKBDmFT4eHoN2ndd6lN370kFiGUFyTiUHWhU7k8="
            crossorigin="anonymous"></script>
    <script src="/js/newItem.js"></script>
</head>

<body>
<div class="adminArea">
<h2>New Item</h2>
    <!--model attribute ne da formatul pe care trebuie sa il caute front endul
     pentru a transpune datele in backend. practic folosim un DTO UserForm cu aceleasi campuri valabile in FE
      si care ajung sa fie cautate prin Path in DTO pt a fi transmise la BE -->
<form:form action="newItem" method="POST" modelAttribute="itemForm">
    <form:input path="itemName" id="itemName" type="text" class="normalInput" placeholder="Item name"/>
    <p class = "error"><form:errors path="itemName"/></p>
    <form:input path="description" id="description" type="text" class="normalInput" placeholder="Product description"/>
    <p class = "error"><form:errors path="description"/></p>
    <form:input path="category" id="category" type="text" class="normalInput" placeholder="Fill in category"/>
        <p class = "error"><form:errors path="category"/></p>
    <form:input path="startingPrice" id="startingPrice" type="number" class="normalInput" placeholder="Enter starting price"/>
        <p class = "error"><form:errors path="startingPrice"/></p>
    <form:input path="startDateForBidding" id="startDateForBidding" type="date" class="normalInput" placeholder="dd-mm-yyyy"/>
    <p class = "error"><form:errors path="startDateForBidding"/></p>
    <form:input path="endDateForBidding" id="endDateForBidding" type="date" class="normalInput" placeholder="dd-mm-yyyy"/>
    <p class = "error"><form:errors path="endDateForBidding"/></p>

<%--    <button name="home" type="submit" text="home">Home</button> --%>

<button name="add_item" type="submit" text="add_item">Add item</button>
<h3>${successMessage}</h3>


</form:form>

<div class="homeArea">
                        <form:form method="get" action="/admin/home/">
                            <button class="button button-header" type="submit">Home
                            </button>
                        </form:form>
                    </div>


</div>
</body>
</html>