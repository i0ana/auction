<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!--*tag pentru a identifica ca noi vom folosi mai jos un formular de Spring, nu unul simplu de html*-->
<!DOCTYPE html>
    <html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title></title>
    <link rel="stylesheet" type="text/css" href="css/loginAndRegister.css">
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
            integrity="sha256-pasqAKBDmFT4eHoN2ndd6lN370kFiGUFyTiUHWhU7k8="
            crossorigin="anonymous"></script>
    <script src="js/login.js"></script>
</head>

<body>
<div class="registerArea">
<h2>Register</h2>
    <!--model attribute ne da formatul pe care trebuie sa il caute front endul
     pentru a transpune datele in backend. practic folosim un DTO UserForm cu aceleasi campuri valabile in FE
      si care ajung sa fie cautate prin Path in DTO pt a fi transmise la BE -->
<form:form action="registration" method="POST" modelAttribute="userForm">
    <form:input path="firstName" id="firstName" type="text" class="normalInput" placeholder="First Name"/>
    <p class = "error"><form:errors path="firstName"/></p>
    <form:input path="lastName" id="lastName" type="text" class="normalInput" placeholder="Last Name"/>
    <p class = "error"><form:errors path="lastName"/></p>
    <form:input path="email" id="email" type="email" class="normalInput" placeholder="Email"/>
    <p class = "error"><form:errors path="email"/></p>
    <form:input path="password" id="password" type="password" class="normalInput" placeholder="Password"/>
    <p class = "error"><form:errors path="password"/></p>
    <form:input path="confirmPassword" id="confirmPassword" type="password" class="normalInput" placeholder="Confirm Password"/>
    <p class = "error"><form:errors path="confirmPassword"/></p>
    <button name="Register" type="submit" text="Register">Submit</button>
    <h3>${successMessage}</h3>

    <ul class="registerFooter">
        <li>
            <a href="login">Log In</a>
        </li>
        <li>
            <a href="#">Forgot Password</a>
        </li>
    </ul>

</form:form>
</div>
</body>
</html>